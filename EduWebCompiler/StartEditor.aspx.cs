﻿//Janine Lee
// 3/12/17

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Script.Services;

namespace EduWebCompiler
{
    public partial class StartEditor : System.Web.UI.Page
    {
        SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-SU8V536\SQLEXPRESS;Initial Catalog=html;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            editor1.Focus();

            //makes sure to get text, before reload
            if (!Page.IsPostBack)
            {
                editor1.Text = OutlineHtml();
                tutorials();
                savedFiles();
            }
            //decodeHtml(), will need to add this later to protect the database;     
        }

        protected void tutorials()
        { 
            //gets right gridview information to go into the editor textbox
            try
            {
                String q1 = Request.QueryString["category"];
                SqlCommand cmd = new SqlCommand("select filename from htmlTable where category='" + q1 + "'", connection);

                connection.Open();
                //uses reader to read from text file for the tutorials
                SqlDataReader readerJ = cmd.ExecuteReader();
                while (readerJ.Read())
                {
                    string h1 = (String.Format("{0}", readerJ[0]));
                    editor1.Text = File.ReadAllText(Server.MapPath("/test files/" +h1));               
                }
                //closes reader and sql connection
                readerJ.Close();
                connection.Close();
            }
            catch (FileNotFoundException ex)
            {
                editor1.Text = "Sorry this was file not found or does not exist.";
                Console.WriteLine(ex);
            }
            catch (Exception ex2)
            {
                editor1.Text = "Error!";
                Console.WriteLine("Tutorial Error "+ex2);
            }
        }

        protected void savedFiles()
        {
            //gets left gridview information to go into the editor textbox
            try
            {
                //uses reader to read from text file for the saved files
                String q2 = Request.QueryString["category_Saved"];
                SqlCommand cmd = new SqlCommand("select filename from savedHtml where category_Saved='" + q2 + "'", connection);

                connection.Open();
                SqlDataReader readerJ = cmd.ExecuteReader();
                
                while (readerJ.Read())
                {
                        string h1 = (String.Format("{0}", readerJ[0]));
                        editor1.Text = File.ReadAllText(Server.MapPath("/test files/" + h1));
                }
                //closes reader and sql connection
                    readerJ.Close();
                    connection.Close();
            }
            catch (FileNotFoundException ex)
            {
                editor1.Text = "Sorry this was file not found or does not exist.";
                Console.WriteLine(ex);
            }
            catch (Exception ex2)
            {
                editor1.Text = "Error!";
                Console.WriteLine("Saved Files Error "+ex2);
            }
        }

        protected String OutlineHtml()
        {
            //What editor has on loadup
            String line1 = "<!DOCTYPE html>";
            String line2 = "    <html>";
            String line3 = "        <head>";
            String title = "        <title> Page Title Here </title>";
            String line4 = "        <h1> Page Header1 </h1>";
            String line5 = "        </head>";
            String line6 = "            <body>";
            String line7 = "            <p>body</p>";
            String line8 = "            </body>";
            String line9 = "    </html>";
                      String all = line1 + "\n" + line2 + "\n" + 
                line3 + "\n" + "\n" + title + "\n" + "\n" +line4 
                + "\n" + line5 + "\n" + "\n" + line6 + "\n" + line7
                + "\n" + "\n" + line8 + "\n" + line9;
            return all;
        }

        protected void reset(object sender, EventArgs e)
        {
            //reset button
            String reset = "";
            editor1.Text = reset;  
        }

        protected void save(object sender, EventArgs e)
        {
            //sql insert: saving files

            if (string.IsNullOrWhiteSpace(saveTextbox.Text))
            {
                //needs to require a name, return if saveTextbox.Text is null or only has blank spaces
                //editor1.Text = "Not a valid name!";
                Console.WriteLine("White space in save textbox");
            }
            //SqlCommand numexists = new SqlCommand("Select count (category_Saved) from savedHtml where (category_Saved = '" + saveTextbox.Text + "')", connection);
            else
            { 
           try
            {
                SqlCommand cmd = new SqlCommand("insert into savedHtml (category_Saved, filename, filelocation) values ('" + saveTextbox.Text +
                    "','" + saveTextbox.Text + ".txt','temp3')", connection);
                
                connection.Open();

                cmd.ExecuteNonQuery();
                connection.Close();                
                //to save over another file, will have to delete old and replace
            }
            catch (Exception sqlEx)
            {
                Console.WriteLine(sqlEx + "Sql Error");
            }

            //save file
            try
            {
                string txt = editor1.Text;
                //string path = Environment.CurrentDirectory;
                string path = AppDomain.CurrentDomain.BaseDirectory;
                //gets text line by line
                string[] saveText = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                //File.WriteAllLines("C:\\temp\\" + saveTextbox.Text + ".txt", saveText);
                File.WriteAllLines(path+ "/test files/" + saveTextbox.Text + ".txt", saveText);
            }  
            catch (FileNotFoundException ex) 
            {
                Console.WriteLine("File not found! /n" + ex);
            }
            catch (Exception ex2)
            {
                Console.WriteLine("Error /n" + ex2);
            }
                //redirects to page with updated saved text
                Response.Redirect("StartEditor.aspx?category_Saved=" + saveTextbox.Text);

            }
        }

    }

}