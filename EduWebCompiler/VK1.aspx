﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VK1.aspx.cs" Inherits="EduWebCompiler.WebForm1" %>

<!Doctype html>

<head>

    <title>HTML5 Editor: Start Here</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="codemirror/codemirror.css"/>
    <link rel="stylesheet" type="text/css" href="codemirror/show-hint.css"/>
    <!--custom style sheets-->
    <link rel="stylesheet" type="text/css" href="MainPage.css"/>

    <!--Made using codemirror: https://codemirror.net/ 
        javascript files -->
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="codemirror/codemirror.js"></script>
    <script type="text/javascript" src="codemirror/xml.js"></script>
    <script type="text/javascript" src="codemirror/javascript.js"></script>
    <script type="text/javascript" src="codemirror/css.js"></script>
    <script type="text/javascript" src="codemirror/htmlmixed.js"></script>
    <script type="text/javascript" src="codemirror/css-hint.js"></script>
    <script type="text/javascript" src="codemirror/html-hint.js"></script>
    <script type="text/javascript" src="codemirror/javascript-hint.js"></script>
    <script type="text/javascript" src="codemirror/show-hint.js"></script>
    <script type="text/javascript" src="codemirror/xml-hint.js"></script>
    
    <script type="text/javascript" src="codemirror/emacs.js"></script>
    <script type="text/javascript" src="codemirror/commands.js"></script>

    <script type="text/javascript" src="codemirror/jump-to-line.js"></script>
    <script type="text/javascript" src="codemirror/search.js"></script>
    <script type="text/javascript" src="codemirror/searchcursor.js"></script>

    <!--keyboard files-->
    <!-- old demo files -->
    
    <link href="keyboard/font-awesome.min.css" rel="stylesheet" type="text/css"  />
    <link href="keyboard/jquery-ui.min.css" rel="stylesheet" type="text/css" />

    <!-- Made using Mottie's keyboard: https://github.com/Mottie/Keyboardwidget 
        jQuery & jQuery UI + theme  -->
    <script type="text/javascript" src="keyboard/bootstrap.min.js"></script>
    <script type="text/javascript" src="keyboard/demo.js"></script>
    <script type="text/javascript" src="keyboard/jquery-ui.min.js"></script>
    <script type="text/javascript" src="keyboard/jquery-latest.min.js"></script>

    <!-- CSS & script  -->
    <link href="centertopbtm.css" rel="stylesheet" type="text/css"/>
    <link href="keyboard/keyboard.css" rel="stylesheet" type="text/css"/>
    
    <script type="text/javascript" src="keyboard/jquery.keyboard.js"></script>
    <link href="VKTest.css" rel="stylesheet" type="text/css" />
    <script src="VKTest.js" type="text/javascript"></script>
    <script src="keyboard/jquery.keyboard.extension-navigation.js" type="text/javascript"></script>

    <!--end of files-->

    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 174px;
        }
        .auto-style3 {
            width: 999px;
        }
        </style>
</head>

<form id="form1" runat="server">

    <article>
        <h2>Coding Website with Accessiblity</h2>
        <h3>Tutorials</h3>

        <!-- initialize keyboard -->
    <script>
		$(function(){
			$('.keyboard').keyboard();
		});
    </script>
    

        <!--start of text editor-->
        <asp:TextBox ID="editor1" runat="server" ValidateRequestMode="Disabled" TextMode="MultiLine"></asp:TextBox>
        <!--end of text editor-->

        <!--Script on preview-->
    <script>
          var delay;
          // Initialize CodeMirror editor with demo.
          var editor = CodeMirror.fromTextArea(document.getElementById('editor1'), {
            mode: 'text/html',
            lineNumbers: true,
            matchBrackets: true,
            //keyMap: "emacs",
		    extraKeys: { "Ctrl-Space": "autocomplete" }
          });
        //-------------------
        //add variables to have keyboard target
          input = editor.getInputField();

    </script>

        <style>
            /*change in Codemirror section to switch to right side*/
            .CodeMirror {
                left: 1000px;
            }

            #leftLinks
            {
                background-color: white;
                color: black;
                float: left;
                width: 33.5%;
                height: 425px;  
                border: 1px solid #231F20;
                position: absolute;
                top: 118px;

                /*font*/
                font-size:16px;
                margin-left:10px;
            }

            #linkname{
                padding-left:10px;
            }

            #linkname input[type='button'] {
                color: black;
                text-decoration: none;
                background: #7EC0EE;
                border-style: none;
                text-align: left;
                outline: none;
                width: 185px;
                height: 30px;
            }
            #linkname input[type='button']:hover {
                background: blue;
            }

            #rightText{
                position:absolute;
                background:white;
                top: 0px;
                left: 202px;
                height: 420px;
                width: 306px;
                resize: none;
                font-family: Helvetica Font Family;
            }

        </style>

        <!--toolbars--> 
        
        <div id=toolbar>
            <p style= "padding-left:4em"> Today's Date: <%= DateTime.Today.ToString("MM/dd/yy") %> </p>
        </div>

        <!--Left panel-->

        <div id="leftLinks">
            <div id="linkname">
              <p></p><asp:button id="homeText" runat="server" Text="Home" OnClick="home" UseSubmitBehavior="false"/>
              <p></p><asp:button id="englishkbLink" runat="server" text="English keyboard" OnClick="engkb" UseSubmitBehavior="false"/> 
              <p></p><asp:button id="htmlkbLink" runat="server" text="HTML keyboard" OnClick="htmlkb" UseSubmitBehavior="false"/>
              <p></p><asp:button id="startHtmlLink" runat="server" text="Getting Started Html" OnClick="starthtml" UseSubmitBehavior="false"/>
              <p></p><asp:button id="cssLink" runat="server" text="CSS keyboard" OnClick="csskb" UseSubmitBehavior="false"/>
              <p></p><asp:button id="startCssLink" runat="server" text="Getting Started CSS" OnClick="startcss" UseSubmitBehavior="false"/>
              <p></p><asp:button id="javascriptLink" runat="server" text="JavaScript keyboard" OnClick="javaskb" UseSubmitBehavior="false"/>
              <p></p><asp:button id="startJavascriptLink" runat="server" text="Getting Started JavaScript" OnClick="startjavas" UseSubmitBehavior="false"/>
            </div>

            <asp:textbox id="rightText" runat="server" TextMode="MultiLine" ValidateRequestMode="Disabled"></asp:textbox>

        </div>

        <!--end of left panel-->

        <div id=toolbar2>
        </div>
        <svg id="svgCircle" width="490" height="427">
          <circle id="circle1" cx="242" cy="212" r="211" stroke="black" stroke-width="1" fill="white" />
          Sorry, your browser does not support inline SVG.  
        </svg> 
        <div>
            <img id="viscardiLogo" src="ImgsVids/HenryViscardiLogo_4C.jpg" />
        </div>

        <!--Radial Virtual Keyboard-->
        <div id=radial_VK1>
        

            <table class="auto-style1">
                <tr>
                    
                    <td class="auto-style3">

                    <script>

                        $.extend($.keyboard.keyaction, {

                            left: function (base) {
                                editor.execCommand('goCharLeft');
                            },
                            right: function (base) {
                                editor.execCommand('goCharRight');
                            }
                        });
                        //with if data-name= X, remove dataaction attribute or set to null, add onclick function
                        if(document.getAttribute("data-name")="meta-aG"){
                            var bksp = "meta-aG";
                            var bksp_action= bksp.getAttribute("data-action")="";
                            bksp.on("click", "button", function(){

                                backOne();
                                });
                            }

                        function backOne() {
                            editor.execCommand("delCharBefore");
                        }

                    </script>

            <!--on button click....delete, select, jumpto-->
                        <button id="TestCmd" type="button" onclick="backOne()">TestCmd</button>

                    </td>

                    <td class="auto-style3">
                        <!--Where Menu would change-->


                    </td>

                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        
        </div>

        
        <div id="socialMediaButtons">
            <!--should just create a few icons, then opens in new tab to save sapce-->
            
        </div>
    </article>
    <!--https://blog.hubspot.com/blog/tabid/6307/bid/29544/the-ultimate-cheat-sheet-for-creating-social-media-buttons.aspx-->
</form>

