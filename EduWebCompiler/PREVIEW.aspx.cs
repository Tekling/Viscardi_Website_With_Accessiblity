﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;

namespace EduWebCompiler
{
    public partial class Preview : System.Web.UI.Page 
    {
        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-SU8V536\\SQLEXPRESS;Initial Catalog=html;Persist Security Info=True;User ID=sa;Password=password!");
        protected void Page_Load(object sender, EventArgs e) 
        {
            
        }

        protected void clickTutorials(object sender, EventArgs e)
        {
            //gets tutorial files
            try
            {
                String qu = Request.QueryString["filename"].ToString();
                TextBox1.Text = File.ReadAllText(Server.MapPath("/test files/"+qu));
            }
            catch (FileNotFoundException ex)
            {
                TextBox1.Text = "File not found! \nRemember to include extensions like .txt";
                Console.WriteLine(ex); 
            }
            catch (Exception ex2)
            {
                TextBox1.Text = "Error!";
                Console.WriteLine(ex2);
            }
        }

        protected void insertHtmlTable(object sender, EventArgs e)
        {
            //sql insert
            if (string.IsNullOrWhiteSpace(categoryText.Text))
            {
                //needs to require a name, return if category.Text is null or only has blank spaces
                TextBox1.Text = "Not a valid name!";
            }
            else
            {
            try
            {
                SqlCommand cmd = new SqlCommand("insert into htmlTable (category) values ('" + categoryText.Text + "')", conn);
                conn.Open();

                cmd.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("Preview.aspx");
            }
            catch (Exception sqlEx)
            {
                Console.WriteLine(sqlEx + "Sql Error");
            }

            }

        }

        protected void clickSavedFiles(object sender, EventArgs e)
        {
            //gets client saved files
            try
            {
                String qu2 = Request.QueryString["filename"].ToString();
                TextBox2.Text = File.ReadAllText(Server.MapPath("/test files/" + qu2));
            }
            catch (FileNotFoundException ex)
            {
                TextBox2.Text = "File not found!";
                Console.WriteLine(ex);
            }
            catch (Exception ex2)
            {
                TextBox2.Text = "Error! " +ex2;
            }
        }
    }
}