﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EduWebCompiler
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //basic guide on using the keyboard
        protected void Page_Load(object sender, EventArgs e)
        {
        //text on page load
            rightText.ReadOnly = true;
            String text1 = "The keyboard types are:"
                + "\n" + "1) The English keyboard"
                + "\n" + "2) The Html keyboard"
                + "\n" + "3) The CSS keyboard"
                + "\n" + "4) The Javascript keyboard"
                + "\n" + "5) The Menu: controls actions for the keyboard"
                + "\n" + "6) Spaces: moves the caret of the editor"
                + "\n" + "7) Navigation: navigates to a certain position on the keyboard"
                + "\n" + "8) Use the center button to return to another keyboard view";
            rightText.Text = text1;

        }

        protected void home(object sender, EventArgs e)
        {
            //text after clicking home toolbar button
            String text1 = "The keyboard types are:"
                + "\n" +"1) The English keyboard"
                + "\n" +"2) The Html keyboard"
                + "\n" +"3) The CSS keyboard"
                + "\n" +"4) The Javascript keyboard"
                + "\n" +"5) The Menu: controls actions for the keyboard"
                + "\n" +"6) Spaces: moves the caret of the editor"
                + "\n" +"7) Navigation: navigates to a certain position on the keyboard"
                + "\n" +"8) Use the center button to return to another keyboard view";
            rightText.Text = text1;
        }

        protected void engkb(object sender, EventArgs e)
        {
            //text after clicking english kb toolbar button
            String text1 = "Click on English Kb+ to access the English keyboard:"
                + "\n" + "-This keyboard is divided into different letter groups and symbols"
                + "\n" + "-It also has a navigational menu and space menu"
                + "\n" + "-To go back, click the center button";
            rightText.Text = text1;
        }

        protected void htmlkb(object sender, EventArgs e)
        {
            //text after clicking html kb toolbar button
            String text1 = "Click HTML kb+ to access. The Html keyboard is divided into different categories:"
                + "\n" + "-Html helps in the creation of your website."
                + "\n" + "1) Links"
                + "\n" + "2) Tables, Lists, Forms"
                + "\n" + "3) The CSS keyboard"
                + "\n" + "4) Text formats"
                + "\n" + "5) Input types"
                + "\n" + "6) Blocks"
                + "\n" + "7) Navigation: navigates to a certain position on the keyboard"
                + "\n" + "8) Use the center button to return to another keyboard view";
            rightText.Text = text1;
        }

        protected void starthtml(object sender, EventArgs e)
        {
            //text after clicking starthtml kb toolbar button
            String text1 = "1) Click on HTML kb+, then Click on Start HTML+ (From the starting keyboard)"
                + "\n" + "      -These following tags will help start your website."
                + "\n" + "2) First define the doctype by clicking the Set Doctype Button... to get <!DOCTYPE html>."
                + "\n" + "3) If you want another line, you can click the center button, then go to the HTML nav button then click 'Enter' for another line."
                + "\n" + "4) Go back to HTML start, then get a header tag by clicking headers+ and then header tag <header>. This is where many of your links to other files will go." 
                + "\n" + "5) Go back using the center button and go to HTML nav, to enter another line."
                + "\n" + "6) Go back to HTML start, then click the Title tag button <title>. The text that goes inbetween this tag, will be the title of the page."
                + "\n" + "7) Repeat the last two steps, but using the body tag <body>."
                + "\n" + "8) Now you have an outline for your website!";
            rightText.Text = text1;
        }

        protected void csskb(object sender, EventArgs e)
        {
            //text after clicking css kb toolbar button
            String text1 = "Click CSS kb+ to access. The CSS keyboard is divided into different categories:"
                + "\n" + "-CSS is used to change the appearance of your website."
                + "\n" + "1) Positioning"
                + "\n" + "2) Boxes"
                + "\n" + "3) Colors"
                + "\n" + "4) Font and Text"
                + "\n" + "5) Movements"
                + "\n" + "6) Animations"
                + "\n" + "7) Navigation: navigates to a certain position on the keyboard"
                + "\n" + "8) Use the center button to return to another keyboard view";
            rightText.Text = text1;
        }

        protected void startcss(object sender, EventArgs e)
        {
            //text after clicking startcss kb toolbar button
            String text1 = "1) Click on CSS kb+, then Click on Start CSS+: (From the starting keyboard)"
               + "\n" + "      -This is an example for styling a webpage."
               + "\n" + "2) First have the page recognize you are using CSS, click style tag... <style>."
               + "\n" + "3) Click the center button, then go to the CSS nav button, to move the caret to the center of the style tag, then click 'Enter' for another line."
               + "\n" + "4) Go back to CSS start, then type in the tag name from HTML, of what you want to style, follow this by brackets."
               + "\n" + "5) You should end up with something like this:  title{}" 
               + "\n" + "6) Go back using the center button and go to CSS nav, to move the caret to the center of the brackets, and press enter."
               + "\n" + "7) Go back to CSS kb, then click Colors+, after click Font Color which will get you color:"
               + "\n" + "8) Click on any color you want, and this should change the title font color ex: color:red;"
               + "\n" + "9) You can repeat steps 4-8 to add more styling to the same tag, or create another tag"
               + "\n" + "10) You can also add id='' to the inside of html tags from Start CSS+, as a selector for the CSS"
               + "\n" + "11) Now you know how to add styling to your website!";
            rightText.Text = text1;
        }

        protected void javaskb(object sender, EventArgs e)
        {
            //text after clicking javascript kb toolbar button
            String text1 = "Click Javascrpt kb+ to access. The JavaScript keyboard is divided into different categories:"
                + "\n" + "-Javascript is scripting programming language used for adding functionality to websites and can change website content."
                + "\n" + "1) Variables, Strings, Objects,..."
                + "\n" + "2) Math functions and symbols"
                + "\n" + "3) Elements"
                + "\n" + "4) Loops"
                + "\n" + "5) Number and String methods"
                + "\n" + "6) Events"
                + "\n" + "7) Navigation: navigates to a certain position on the keyboard"
                + "\n" + "8) Use the center button to return to another keyboard view";
            rightText.Text = text1;
        }

        protected void startjavas(object sender, EventArgs e)
        {
            //text after clicking start javascript kb toolbar button
            String text1 = "1) For this example, we are going to be changing some text from html."
               + "\n" + "      -This is an example calling an element from html, and changing it."
               + "\n" + "2) Click the center button, then go to the HTML kb+ and add a paragraph or <p> tag with some random text inbetween."
               + "\n" + "3) Navigate to the front of the p tag, go to CSS kb+>start CSS> id='', Navigate inbetween the quote and name it test"
               + "\n" + "4) Click the center button, then go to the HTML kb+ and add a paragraph or <p> tag with some random text inbetween."
               + "\n" + "5) Click on Javascrpt kb+, then Click on Start JS+: (From the starting keyboard)"
               + "\n" + "6) To have the page recognize you are using javascript, click on scriptHTML to get the script tag...<script>"
               + "\n" + "7) Go back using the center button and go to JS nav, to move the caret to the center of the tag, and press enter."
               + "\n" + "8) Stay in the JS Nav view and move the cursor to a place outside the tags."
               + "\n" + "9) Go to the HTML kb>Inputs+>Buttons, this creates a button... <input type='button'>, if you want you can rename the button."
               + "\n" + "10) Navigate to somewhere inbetween the input part of the tag and the equals sign>go to Javascrpt kb+>events>onclick=''...this gets onclick=''."
               + "\n" + "11) Navigate inbetween the onclick quotation marks, and add some text using the English kb+."
               + "\n" + "12) Navigate back inbetween the script tags>Javascrpt kb+>Var,objects+>function()."
               + "\n" + "13) Navigate between function and the front parenthesis, add a space."
               + "\n" + "14) , then name it using the Enligsh kb+ to the ****exact same as what you put inbetween the onclick quotes."
               + "\n" + "15) Add brackets right after the function name() {}, then navigate inbetween those brackets."
               + "\n" + "16) Go back to the Javascrpt kb+>Elements+>getElem+>getIdHtml...document.getElementById('');"
               + "\n" + "17) Navigate to inside the quotation makes of the getElementId and type in test(), this will call upon the paragraph tag that you had an id named before for it"
               + "\n" + "18) Go back to the JS keyboard> Elements+>Change Elem+>.innerHtml=;....innerHtml=;"
               + "\n" + "19) Navigate inbetween the equals sign and semi-colon, and type in any text."
               + "\n" + "20) On the click of the button, the text for the paragraph should change to the text in step 19.";
            rightText.Text = text1;
        }
    }
}