﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="StartEditor.aspx.cs" Inherits="EduWebCompiler.StartEditor" %>

<!--Janine Lee
    5/7/17-->

<!Doctype html>

<head>

    <title>HTML5 Editor</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="codemirror/codemirror.css"/>
    <link rel="stylesheet" type="text/css" href="codemirror/show-hint.css"/>
    <!--custom style sheets-->
    <link rel="stylesheet" type="text/css" href="MainPage.css"/>

    <!--Made using codemirror: https://codemirror.net/ 
        javascript files -->
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="codemirror/codemirror.js"></script>
    <script type="text/javascript" src="codemirror/xml.js"></script>
    <script type="text/javascript" src="codemirror/javascript.js"></script>
    <script type="text/javascript" src="codemirror/css.js"></script>
    <script type="text/javascript" src="codemirror/htmlmixed.js"></script>
    <script type="text/javascript" src="codemirror/css-hint.js"></script>
    <script type="text/javascript" src="codemirror/html-hint.js"></script>
    <script type="text/javascript" src="codemirror/javascript-hint.js"></script>
    <script type="text/javascript" src="codemirror/show-hint.js"></script>
    <script type="text/javascript" src="codemirror/xml-hint.js"></script>
    
    <script type="text/javascript" src="codemirror/emacs.js"></script>
    <script type="text/javascript" src="codemirror/commands.js"></script>

    <script type="text/javascript" src="codemirror/jump-to-line.js"></script>
    <script type="text/javascript" src="codemirror/search.js"></script>
    <script type="text/javascript" src="codemirror/searchcursor.js"></script>

    <!--keyboard files-->

    <title>Virtual Keyboard with HTML Coding</title>
    <!-- old demo files -->
    
    <link href="keyboard/font-awesome.min.css" rel="stylesheet" type="text/css"  />
    <link href="keyboard/jquery-ui.min.css" rel="stylesheet" type="text/css" />

    <!-- Made using Mottie's keyboard: https://github.com/Mottie/Keyboardwidget 
        jQuery & jQuery UI + theme  -->
    <script type="text/javascript" src="keyboard/bootstrap.min.js"></script>
    <script type="text/javascript" src="keyboard/demo.js"></script>
    <script type="text/javascript" src="keyboard/jquery-ui.min.js"></script>
    <script type="text/javascript" src="keyboard/jquery-latest.min.js"></script>

    <!-- CSS & script  -->
    <link href="centertopbtm.css" rel="stylesheet" type="text/css"/>
    <link href="keyboard/keyboard.css" rel="stylesheet" type="text/css"/>
    
    <script type="text/javascript" src="keyboard/jquery.keyboard.js"></script>
    <link href="VKTest.css" rel="stylesheet" type="text/css" />
    <script src="VKTest.js" type="text/javascript"></script>
    <script src="keyboard/jquery.keyboard.extension-navigation.js" type="text/javascript"></script>

    <!--end of files-->

    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 174px;
        }
        .auto-style3 {
            width: 999px;
        }
        .auto-style5 {
            width: 104px;
        }
    </style>
</head>

<form id="form1" runat="server">

    <article>
        <h2>Coding Website with Accessibility</h2>
        <h3>HTML5 Editor</h3>

        <!-- initialize keyboard -->
    <script>
		$(function(){
			$('.keyboard').keyboard();
		});
    </script>
    

        <!--start of text editor-->
        <asp:TextBox ID="editor1" runat="server" ValidateRequestMode="Disabled" TextMode="MultiLine"></asp:TextBox>
        <!--end of text editor-->
    
        <!--start of preview-->
        <iframe id=preview></iframe>
        <!--end of preview-->

        <!--Script on preview-->
    <script>
          var delay;
          // Initialize CodeMirror editor with demo.
          var editor = CodeMirror.fromTextArea(document.getElementById('editor1'), {
            mode: 'text/html',
            lineNumbers: true,
            matchBrackets: true,
            //keyMap: "emacs",
		    extraKeys: { "Ctrl-Space": "autocomplete" }
          });
        //-------------------
        //add variables to have keyboard target
          input = editor.getInputField();
          //var  otherInput = document.querySelectorAll('[kbTarget="true"]');
          //var allKbs = [input, otherInput];

          editor.on("change", function() {
            clearTimeout(delay);
            delay = setTimeout(updatePreview, 300);
          });
          //updates preview
          function updatePreview() {
            var previewFrame = document.getElementById('preview');
            var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
            preview.open();
            preview.write(editor.getValue());
            preview.close();
          }
          setTimeout(updatePreview, 300);
    </script>

        <!--toolbars--> 
        
        <div id=toolbar>
            <p style= "padding-left:4em"> Today's Date: <%= DateTime.Today.ToString("MM/dd/yy") %> </p>
        </div>

        <div id=toolbar2>
        </div>
        <svg id="svgCircle" width="490" height="427">
          <circle id="circle1" cx="242" cy="212" r="211" stroke="black" stroke-width="1" fill="white" />
          Sorry, your browser does not support inline SVG.  
        </svg> 
 
        <div>
            <img id="viscardiLogo" src="ImgsVids/HenryViscardiLogo_4C.jpg" />
        </div>

        <!--Radial Virtual Keyboard-->
        <div id=radial_VK1>
        

            <table class="auto-style1">
                <tr>
                    <td class="auto-style5">
                        <asp:Button ID="Save" runat="server" OnClick="save" Text="Save" />
                        <asp:TextBox ID="saveTextbox" runat="server"></asp:TextBox>
                    </td>
                    <td class="auto-style3">

                    <script>

                        $.extend($.keyboard.keyaction, {

                            left: function (base) {
                                editor.execCommand('goCharLeft');
                            },
                            right: function (base) {
                                editor.execCommand('goCharRight');
                            }
                        });
                        //with if data-name= X, remove dataaction attribute or set to null, add onclick function
                        if(document.getAttribute("data-name")="meta-aG"){
                            var bksp = "meta-aG";
                            var bksp_action= bksp.getAttribute("data-action")="";
                            bksp.on("click", "button", function(){

                                backOne();
                                });
                            }

                        function backOne() {
                            editor.execCommand("delCharBefore");
                        }

                    </script>

            <!--on button click....delete, select, jumpto-->
                        <button id="TestCmd" type="button" onclick="backOne()">TestCmd</button>

                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2"  ShowHeader="False">
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="category_Saved" DataNavigateUrlFormatString="StartEditor.aspx?category_Saved={0}" DataTextField="category_Saved" ShowHeader="False" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:htmlConnectionString5 %>" SelectCommand="SELECT [category_Saved] FROM [savedHtml]"></asp:SqlDataSource>
                    </td>

                    <td class="auto-style3">
                        <!--Where Menu would change-->

                        <div id="jumpToLine">
                        <p id="jumpText">Jump to line:</p> <!--Jump to box-->
                            <p id="jumpTextHint">line#:char#</p>
                        <asp:TextBox ID="jumpBox" runat="server" kbTarget="true"></asp:TextBox>
                            <p>
                                <!--switch between menus-->
                                <asp:ImageButton ID="menuSwitch" runat="server" ImageUrl="ImgsVids/VK 1-1.png" />
                            </p>
                            <p>
                                <!--accept button-->
                                <asp:Button ID="jumpAccept" runat="server" Text="Accept" />
                            </p>
                            <p>
                                <!--back to keyboard-->
                                <asp:Button ID="jumpBack" runat="server" Text="Back" />
                            </p>
                        </div>

                    </td>

                    <td>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" ShowHeader="False" OnRowDataBinding="GridView1_DataBind">
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="category" DataNavigateUrlFormatString="StartEditor.aspx?category={0}" DataTextField="category" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:htmlConnectionString4 %>" SelectCommand="SELECT [category] FROM [htmlTable]"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        &nbsp;</td>
                    <td class="auto-style3">

                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Preview.aspx">Manage</asp:HyperLink>
                    </td>

                    <td class="auto-style3">
                        &nbsp;</td>

                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        
        </div>

        <!--testing adding new buffers....-->
        <!--
            <select id="buffers_top">
                <option>js</option><option>css</option>
            </select>
            <button onclick="newBuf('bot')">New buffer</button>

            <script id="script">   --
                var sel_top = document.getElementById("buffers_top");
                CodeMirror.on(sel_top, "change", function () {
                    selectBuffer(ed_top, sel_top.options[sel_top.selectedIndex].value);
                });

                var buffers = {};

                function openBuffer(name, text, mode) {
                    buffers[name] = CodeMirror.Doc(text, mode);
                    var opt = document.createElement("option");
                    opt.appendChild(document.createTextNode(name));
                    sel_top.appendChild(opt);
                    sel_bot.appendChild(opt.cloneNode(true));
                }

                function newBuf(where) {
                    var name = prompt("Name for the buffer", "*new buffer1*");
                    if (name == null) return;
                    if (buffers.hasOwnProperty(name)) {
                        alert("There's already a buffer by that name.");
                        return;
                    }
                    openBuffer(name, "", "javascript");
                    selectBuffer(where == "top" ? ed_top : ed_bot, name);
                    var sel = where == "top" ? sel_top : sel_bot;
                    sel.value = name;
                }

                function selectBuffer(editor, name) {
                    var buf = buffers[name];
                    if (buf.getEditor()) buf = buf.linkedDoc({ sharedHist: true });
                    var old = editor.swapDoc(buf);
                    var linked = old.iterLinkedDocs(function (doc) { linked = doc; });
                    if (linked) {
                        // Make sure the document in buffers is the one the other view is looking at
                        for (var name in buffers) if (buffers[name] == old) buffers[name] = linked;
                        old.unlinkDoc(linked);
                    }
                    editor.focus();
                }

                function nodeContent(id) {
                    var node = document.getElementById(id), val = node.textContent || node.innerText;
                    val = val.slice(val.match(/^\s*/)[0].length, val.length - val.match(/\s*$/)[0].length) + "\n";
                    return val;
                }
                openBuffer("js", nodeContent("script"), "javascript");
                openBuffer("css", nodeContent("style"), "css");

                selectBuffer(editor, "js");                
                </script>
            -->
        <!--end of buffers test--> 

        <!--Social media buttons go here-->
        
        <div id="socialMediaButtons">
            <!--should just create a few icons, then opens in new tab to save sapce-->
            
        </div>
    </article>
    <!--https://blog.hubspot.com/blog/tabid/6307/bid/29544/the-ultimate-cheat-sheet-for-creating-social-media-buttons.aspx-->

</form>

