﻿/* VIRTUAL KEYBOARD DEMO - https://github.com/Mottie/Keyboard 
Main Keyboard (Central Website)*/

$(function () {
    
    $.keyboard.language.love = $.extend($.keyboard.language.en);

    $(input).keyboard({
        //since keyboard is set to always open, will need to set to "keyboard next" to switch between menus

        // language defaults to ["en"] if not found
        language: ['love'],
        rtl: false,
        layout: 'custom',
        customLayout: {

            //to highlight onebyone for a few seconds each, to allow selections


            //think about each keyboard having frequent keys like ;
            //debate should go back to previous menu?--should always go back to second sub menu?
            //maybe an identifier to show whether it goes to a new menu
            'default': [
               //Starting keyboard       
                   '{meta-menu} {meta-abc} {meta-html}',
                   '{meta-space}  {meta-kbs}  {meta-javas}',
                   '{meta-nav} {meta-actions} {meta-css}',

            ],
            //abc
            'meta-abc': [
                 '{meta-menu} {meta-aG} {meta-hN}',
                 '{meta-nav2} {default} {meta-oU}', //seperate navs
                 '{meta-sp2} {meta-1_10sp1} {meta-vZ}',

            ],

            'meta-aG': [
              '{meta-aGS} a b',
              'g {meta-abc} c',
              'f e d',
              
            ],
            //shift A-G
            'meta-aGS': [
               '{meta-aG} A  B',
               'G {meta-abc} C',
               'F E D',
               ,
            ],
            'meta-hN': [
                  '{meta-hNS} h i',
                  'n {meta-abc} j',
                  'm l k',

            ],
            //shift H-N
            'meta-hNS': [
                  '{meta-hN} H I',
                  'N {meta-abc} J',
                  'M L K',
            ],
            'meta-oU': [
                   '{meta-oUS} o p',
                   'u {meta-abc} q',
                   't s r',
            ],
            //shift O-U
            'meta-oUS': [
                '{meta-oUS} O P',
                'U {meta-abc} Q',
                'T S R',
            ],
            'meta-vZ': [
                   '{meta-vZS} v w',
                   '> {meta-abc} x',
                   '< z y',
            ],
            //shift V-Z and ()
            'meta-vZS': [
                  '{meta-vZS} V W',
                  ') {meta-abc} X',
                  '( Z Y',
            ],

            //aphabet keys ends
            //numbers begin (1-7)
            'meta-1_10sp1': [
                  '{meta-1_10sp1S} 1 2',
                  '7 {meta-abc} 3',
                  '6 5 4',
            ],
            //shift numbers (8-9) and symbols1
            'meta-1_10sp1S': [
                '{meta-1_10sp1} 8 9',
                '; {meta-abc} 0',
                ': # =',
            ],
            //numbers end and symbols con.2
            'meta-sp2': [
                  '{meta-sp2S} . {',
                  "' {meta-abc} }",
                  ', ? !',
            ],
            //symbols2 shift
            'meta-sp2S': [
                 '{meta-sp2} + -',
                 '" {meta-abc}  *',
                 '% [] /',
            ],


            //navs
            //primary nav
            'meta-nav': [
                               '{enter} {space} {cmBksp}', //probably should make seperate arrows
                               '{cmJump}  {default}  {right}', 
                               '{cmLineDown} {cmLineUp} {left}', 
            ],
            //english nav
            'meta-nav2': [
                               '{enter} {space} {cmBksp}', //probably should make seperate kb (left/right&up/down)
                               '{cmJump}  {meta-abc}  {right}', //should add button for common keys
                               '{cmLineDown} {cmLineUp} {left}',
            ],
            //html nav
            'meta-nav3': [
                               '{enter} {space} {cmBksp}', //probably should make seperate arrows
                               '{cmJump}  {meta-html}  {right}',
                               '{cmLineDown} {cmLineUp} {left}',
            ],
            //css nav
            'meta-nav4': [
                               '{enter} {space} {cmBksp}', //probably should make seperate arrows
                               '{cmJump}  {meta-css}  {right}',
                               '{cmLineDown} {cmLineUp} {left}',
            ],
            //javascript nav
            'meta-nav5': [
                               '{enter} {space} {cmBksp}', //probably should make seperate arrows
                               '{cmJump}  {meta-javas}  {right}',
                               '{cmLineDown} {cmLineUp} {left}',
            ],

            //spaces
            'meta-space': [
                   '{enter} {space} {cmBksp}', //later will make seperate keyboard for space/bsp
                   '{tab}  {default}  {cmDeleteL}', 
                   '{meta-pm1} {meta-pm2} {cmDeleteRight}',    //select&copy
            ],

            'meta-actions': [
                    '{meta-switchView} {cmUndo} {cmRedo}', //search and replace //select-copy-paste
                    '{meta-email}  {default}  {cmReset}', 
                    '{meta-facebook} {meta-twitter} {meta-searchReplace}',
            ],
            //post to social media
            'meta-twitter': [
                    '{empty:3.5}{meta-Tweet}{empty:3.5}',
                    '{empty:3.5} {default} {empty:3.5}',
                    '{empty:3.5}{meta-TwitterFollow}{empty:3.5}',
            ],
            'meta-facebook': [
                    '{empty:3.5}{meta-Comment}{empty:3.5}',
                    '{empty}  {default}  {FacebookFollow} ',
                    '{empty:3.5}{meta-Like}{empty:3.5}',
            ],
            //menu and actions 
            'meta-menu': [
                    '{cmAutocomplete} {meta-tut} {meta-save}',
                    '{meta-select}  {default}  {meta-open}', //select by line, word, brackets
                    '{meta-morekeys} {meta-otherkeys} {meta-reset}',   //id, classes?
            ],
            'meta-otherkeys': [
                    '{meta-menu} _ &',
                    "'  {default}  $",
                    '~ \\ ^', //one '\' only
            ],
            'meta-morekeys': [
                      '{meta-menu} | [',
                      '☹  {default}  ]',
                      '☺ π `',
            ],

            //html keyboard -blocks, id, script, media, tables 
            'meta-html': [
                    '{meta-startHtml} {linkHtml} {meta-tableList}',
                    '{meta-nav3}  {default}  {meta-formating}',   //link other sheets?
                    '{meta-media} {meta-block} {meta-input}',
            ],
            'meta-tableList': [
                   '{empty:3.5} {meta-table} {empty:3.5}',
                   '{empty:3.5}  {meta-html}  {empty:3.5}',
                   '{empty:3.5} {meta-list} {empty:3.5}',
            ],
            'meta-table': [
                       '{empty:3.5} {tableHtml} {empty:3.5}',
                       '{empty:3.5}  {meta-html}  {tableCol}',
                       '{empty:3.5} {tableRow} {empty:3.5}',
            ],

            'meta-list': [
                       '{empty:3.5} {listUO} {listItem}',
                       '{empty:3.5}  {meta-html}  {listOU}', //special list types later?
                       '{empty:3.5} {empty:3.5} {empty:3.5}',
            ],

            'meta-input': [
                    '{meta-inputOthers} {buttonHtml} {inputText}',
                    '{inputNum}  {meta-html}  {inputSubmit}', //do other inputs?
                    '{inputEmail} {inputCheckbox} {inputRadio}',
            ],

            'meta-block': [
                    '{empty:3.5} {divHtml} {spanHtml}',
                    '{codeHtml}  {meta-html}  {textareaHtml}', //do other?
                     '{lineBreak} {highlightHtml} {commentHtml}',
            ],

            'meta-formating': [
                    '{supersubHtml} {boldHtml} {italicHtml}',
                    '{subscriptHtml}  {meta-html}  {quoteHtml}', //doubles empty# space...7
                    '{insertedHtml} {delHtml} {smallHtml}',
            ],

            'meta-media': [
                     '{html5Supp} {imageHtml} {videoHtml}',
                     '{svgHtml}  {meta-html}  {audioHtml}', //combine videoandaudio?, move link
                     '{canvasHtml} {iframeHtml} {objectHtml}',
            ],
                    'meta-action': [
                             '{enter} {codeHtml} {empty:3.5}',
                             '{empty:3.5}  {meta-html}  {empty:3.5}', //html entities? //remove? 
                             '{empty:3.5} {empty:3.5} {empty:3.5}',
                    ],

            'meta-startHtml': [
                    '{enter} // {meta-header}',
                    '{doctype}  {meta-html}  {titleHtml}',
                    '{htmlNew} {pHtml} {bodyHtml}',
            ],

            'meta-header': [
                           '{enter} {headHtml} {head1}',
                           '{head6}  {meta-html}  {head2}',
                           '{head5} {head4} {head3}',
            ],
            //css keyboard   --color,position,animation
            'meta-css': [                                               
                    '{meta-moreCSS} {meta-position} {meta-Boxes}',    
                    '{meta-nav4}  {default}  {meta-color}',
                    '{meta-animate} {meta-transfTransit}  {meta-fontText}', 
            ],                                                      // 
            'meta-position': [
                    '{meta-positionType} {positionCSS} {leftCSS}',
                    '{widthCSS}  {meta-css}  {rightCSS}', //left and right not func properly
                    '{heightCSS} {bottomCSS} {topCSS}',
            ],
                    'meta-positionType': [
                                    '{empty:3.5} absolute; inline-block;',
                                    '{zindexCSS}  {meta-css}  static;',
                                    '{floatCSS} fixed; relative;',
                    ],
            'meta-Boxes': [ 
                    '{empty:3.5} {marginCSS} {empty:3.5}',
                    '{meta-outline}  {meta-css}  {paddingCSS}',   //margin-top/btm....?
                    '{empty:3.5} {borderCSS} {empty:3.5}',  //more to border
            ],
                    'meta-outline': [
                             '{meta-moreOutline} {outlineStyleCSS} dotted;',
                             '{outlineColorCSS}  {meta-css}  dashed;',   //margin-top/btm....?
                             'groove; double; solid;',
                    ],
                            'meta-moreOutline': [
                                 '{empty:3.5} {empty:3.5} {empty:3.5}',
                                 '{empty:3.5}  {meta-css}  {empty:3.5}',   //margin-top/btm....?
                                 '{empty:3.5} {empty:3.5} {empty:3.5}',
                            ],
            'meta-color': [
                    '{meta-moreColor} {colorCSS} red;',
                    'black;  {meta-css}  orange;', //other colors? sample hex?
                    'blue; green; yellow;',
            ],
                    'meta-moreColor': [
                                    '{empty:3.5} white; {empty:3.5}',
                                    '{empty:3.5}  {meta-css}  {empty:3.5}', //other colors? sample hex?
                                    '{empty:3.5} cyan; {empty:3.5}',
                    ],
            'meta-fontText': [ 
                    '{empty:3.5} {fontCSS} {empty:3.5} ',
                    '{meta-textType}  {meta-css}  {textCSS} ',
                    '{empty:3.5} {meta-fontType} {empty:3.5} ',
            ],
                    'meta-fontType': [
                                   '{empty:3.5} {fontCSS} {fontFamCSS}',
                                   '{empty:3.5}  {meta-css}  {fontSizeCSS}',
                                   '{fontVarCSS} {fontWeightCSS} {fontStyleCSS}',
                    ],
                    'meta-textType': [
                                    '{meta-textAlign} {textIndent} {textDecor}',
                                    '{empty:3.5}  {meta-css}  {textShadow}',
                                    '{meta-textSpace} {textOver} {textTransform}',
                    ],
                    'meta-textAlign': [
                                    '{empty:3.5} {textAlign} {direcCSS}',
                                    '{empty:3.5}  {meta-css}  {verticalAlign}',
                                    '{empty:3.5} {empty:3.5} {unicodeBidi}',
                    ],
                    'meta-textSpace': [
                                    '{empty:3.5} {letterSpace} {wordSpace}',
                                    '{empty:3.5}  {meta-css}  {whiteSpace}',
                                    '{empty:3.5} {empty:3.5} {empty:3.5}',
                    ],
                    'meta-hover': [
                               '{empty:3.5} {hoverCSS} {empty:3.5}',                   //removed
                               '{empty:3.5}  {meta-css}  {empty:3.5}',
                               '{empty:3.5} {empty:3.5} {empty:3.5}',
                    ],
            'meta-animate': [
                        '{hoverCSS} @keyframes {animationCSS}',
                        'box-shadow  {meta-css}  {animationDelay}',
                        '{animationName} {animationDuration} {animationDirec}',  //not all
            ],
            'meta-transfTransit': [
                        '{empty:3.5} {transTCSS} {meta-transition}',
                        '{empty:3.5}  {meta-css}  {transFCSS}',
                        '{empty:3.5} {meta-transform3D} {meta-transform}',
            ],
            'meta-transition': [
                  '{empty:3.5} {transTDelay} {empty:3.5}',
                  '{transTTimin}  {meta-css}  {transTDura}',
                  '{empty:3.5} {transTProp} {empty:3.5} ',
            ],
            'meta-transform': [
                 '{empty:3.5} {transFOrigin} {transFStyle}',
                 '{empty:3.5}  {meta-css}  {perspectCSS}',
                 '{empty:3.5} {backfaceVisible} {perspectOrigin}',   
            ],
            'meta-transform3D': [
                             '{empty:3.5} {transFCSS} {meta-translate}',
                             '{meta-skew}  {meta-css}  {meta-scale}',
                             'matrix3d(); perspective(); {meta-rotate}',
            ],
                            'meta-translate': [
                                                '{empty:3.5} translate3d(); translateX();',
                                                '{empty:3.5}  {meta-css}  translateY();',
                                                '{empty:3.5} {empty:3.5} translateZ();',
                            ],
                            'meta-scale': [
                                                '{empty:3.5} scale3d(); scaleX();',
                                                '{empty:3.5}  {meta-css}  scaleY();',
                                                '{empty:3.5} {empty:3.5} scaleZ();',
                            ],
                            'meta-rotate': [
                                                 '{empty:3.5} {transTCSS} {meta-transition}',
                                                 '{empty:3.5}  {meta-css}  {empty:3.5}',
                                                 '{empty:3.5} {empty:3.5} {empty:3.5}',
                            ],
                            'meta-skew': [
                                                 '{empty:3.5} skew(); skewX(angle);',
                                                 '{empty:3.5}  {meta-css}  skewY();',
                                                 '{empty:3.5} {empty:3.5} {empty:3.5}',
            ], 
            'meta-moreCSS': [
                        '} id="" #',   //css func&selectors
                        '{  {meta-css}  {styleTag} ',
                        '{} function()  {meta-visiblity}',    //blend
            ],
            'meta-visiblity': [
                 '{empty:3.5} visible; hidden;',   //css func&selectors
                 '{empty:3.5}  {meta-css}  {opacityCSS}',
                 '{empty:3.5} {empty:3.5} {filterCSS}',    
            ],
                    'meta-webkit': [
                                    '{empty:3.5} {empty:3.5} {empty:3.5}',
                                    '{empty:3.5}  {meta-css}  {empty:3.5} ',  //webkit box shadow  //removed 
                                    '{empty:3.5} {empty:3.5} {empty:3.5};',
                    ],
            //javascript keyboard --variables, math, loops, elements, events/time, apis, functions, methods
            'meta-javas': [   
                    '{meta-moreJS} {meta-varString} {meta-math}',
                    '{meta-nav5}  {default}  {meta-elements}', 
                    '{meta-events} {meta-methods} {meta-loops}',
            ],
            'meta-varString': [
                    '; var; {stringJS}',
                    '{scriptHTML}  {meta-javas}  {varJS}',
                    'function() Boolean; {arrayJS}',
            ],
            'meta-elements': [
                    '{empty:3.5} {meta-getElements} {empty:3.5} ',
                    'id=""  {meta-javas}  {meta-changeElements}',
                    '{empty:3.5} {meta-addDeleteElements} {empty:3.5}',
            ],
                    'meta-getElements': [
                            '{empty:3.5}  {getIdHTML} {empty:3.5}',
                            ';  {meta-javas}  {getTagHTML}',
                            '{empty:3.5} {getClassHTML} {empty:3.5}',
                    ],
                    'meta-changeElements': [
                            '; .innerHTML=; .attribute=;',
                            '{scriptHTML}  {meta-javas}  .setAttribute(,)',
                            '{empty:3.5} {empty:3.5} .style.property=;',
                    ],
                    'meta-addDeleteElements': [
                                            '; .createElement(); .removeChild()',
                                            '{scriptHTML}  {meta-javas}  .appendChild()',
                                            '{empty:3.5} .write() .replaceChild()',
                    ],
            'meta-math': [
                    '{empty:3.5} {meta-mathOperators} {empty:3.5}',
                    '{empty:3.5}  {meta-javas}  {meta-mathComparisons}',
                    '{empty:3.5} {meta-mathFunc} {empty:3.5}',
            ],
                    'meta-mathOperators': [
                            '{empty:3.5} - +',                       //need to check if chars recognized
                            '/  {meta-javas}  =',
                            '* != ==',
                    ],
                    'meta-mathComparisons': [
                            '{empty:3.5} ++ --',
                            '||  {meta-javas}  >',
                            '<= < >=',
                    ],
                    'meta-mathFunc': [
                            '{empty:3.5} pow(); sqrt();',
                            'tan()  {meta-javas}  abs()',
                            'cos() sin() random()',
                    ],
            'meta-loops': [
                    '{empty:3.5} {forLoop} {whileLoop}',
                    '{meta-returnType}  {meta-javas}  {switchJS}',
                    '{tryCatchLoop} {dowhileLoop} break;',
            ],
                    'meta-return': [
                            '{empty:3.5} return true;',
                            'in  {meta-javas}  false;',
                            'finally throw break;',
                    ],
            'meta-methods': [
                            '{empty:3.5} {meta-stringMethods} {empty:3.5} ',
                            '{empty:3.5}  {meta-javas}  {empty:3.5}',
                            '{empty:3.5} {meta-numMethods} {empty:3.5}',
            ],
                        'meta-stringMethods': [
                                        '.split(); .length; .indexOf();',
                                        '.chatAt();  {meta-javas}  .search;',
                                        '.concat(); .replace(); .slice();',
                        ],
                        'meta-numMethods': [
                                        '{meta-numProp} .toExponential(); .toFixed();',
                                        '.parseFloat();  {meta-javas}  .toPrecision();',
                                        '.parseInt; Number(); .valueOf();',
                        ],
                                        'meta-numProp': [
                                                '{empty:3.5} MAX_VALUE MIN_VALUE {empty:3.5}',
                                                '{empty:3.5}  {meta-javas}  NEGATIVE_INFINITY',
                                                '{empty:3.5} NaN POSITIVE_INFINITY {empty:3.5}' ,
                                        ],
            'meta-events': [
                            '{meta-domEvents} onclick="" onchange=""',
                            'event  {meta-javas}  onmouseover=""',
                            'onload onkeydown="" onmouseover=""',
            ],
                            'meta-domEvents': [
                                            '{empty:3.5} {meta-onmouse} {meta-onkey}',
                                            'event  {meta-javas}  onmouseover=""',
                                            'onload onkeydown="" onmouseover=""',
                            ],
                                            'meta-onmouse': [
                                                            'onmouseup ondblclick(); onmousedown;',
                                                            'onmouseout  {meta-javas}  onmouseenter();',
                                                            'onmouseover onmousemove onmouseleave',
                                            ],
                                            'meta-onkey': [
                                                            '{empty:3.5} onkeydown="" onkeypress=""',
                                                            '{empty:3.5}  {meta-javas}  onkeyup=""',
                                                            '{empty:3.5} {empty:3.5} {empty:3.5}',
                                            ],
            'meta-moreJS': [
                            '{empty:3.5} {scriptHTML} undefined;',
                            '{meta-advJS}  {meta-javas}  null;',
                            'clearInterval(); setTimeout(); setInterval();',
            ],
                            'meta-advJS': [
                                            '{empty:3.5} document.cookie=; localStorage.setItem();',
                                            '{empty:3.5}  {meta-javas}  alert();',
                                            '{empty:3.5} blur(); focus();',
                            ],
        },
        // Used by jQuery UI position utility
        position: {
            // null = attach to input/textarea;
            // use $(sel) to attach elsewhere
            of: $("#wrap"),
            my: 'center top',
            at: 'center top',
            // used when "usePreview" is false
            at2: 'center bottom'
        },

        // allow jQuery position utility to reposition the keyboard on
        // window resize
        reposition: true,

        // true: preview added above keyboard;
        // false: original input/textarea used
        usePreview: false,

        // if true, the keyboard will always be visible
        alwaysOpen: true,

        // give the preview initial focus when the keyboard
        // becomes visible
        initialFocus: true,
        // Avoid focusing the input the keyboard is attached to
        noFocus: false,

        // if true, keyboard will remain open even if
        // the input loses focus.
        stayOpen: false,

        // Prevents the keyboard from closing when the user clicks or
        // presses outside the keyboard. The `autoAccept` option must
        // also be set to true when this option is true or changes are lost
        userClosed: false,

        // if true, keyboard will not close if you press escape.
        ignoreEsc: false,

        // *** change keyboard language & look ***
        display: {

            
            //abc keyboard
            'default': 'Start kb',

            'meta-space': 'Spaces+',
            'meta-nav': 'Navigation+',
            'meta-actions': 'Actions+',
            'meta-menu': "Menu...+",
            'meta-autocomplete': 'Autocomplete',
            'meta-otherkeys': 'Other keys+',
            'meta-morekeys': 'More keys+',

            'meta-kbs': 'Keyboards',
            'meta-abc': 'English Kb+',

            'meta-aG': 'a,b,c,d,e,f,g ',
            'meta-hN': 'h,i,j,k,l,m,n',
            'meta-oU': 'o,p,q,r,s,t,u',
            'meta-vZ': 'v,w,x,y,z,<,> ()',
            'meta-1_10sp1': 'Num,=,#,:,;',
            'meta-sp2': 'Math,!," ?,{},[],,,.',

            //shifts
            'meta-aGS': 'Shift ',
            'meta-hNS': 'Shift',
            'meta-oUS': 'Shift',
            'meta-vZS': 'Shift ()',
            'meta-1_10sp1S': '8,9,0,=,#,:,;',
            'meta-sp2S': '?,{},[],,,.',

            //html keyboard  
            
            'meta-html': 'HTML kb+',
            'meta-startHtml': 'Start HTML+',
                'meta-header': 'headers+',
                    'headHtml': 'header tag',
                    'head1': 'h1 tag',
                    'head2': 'h2 tag',
                    'head3': 'h3 tag',
                    'head4': 'h4 tag',
                    'head5': 'h5 tag',
                    'head6': 'h6 tag',
                    'doctype': 'Set Doctype',
                    'titleHtml': 'Title tag',
                    'htmlNew': 'Html tag',
                    'pHtml': 'Paragraph',
                    'bodyHtml': 'Body tag',
            'meta-moreHTML': 'Nav, Symbol+',
            'linkHtml': 'Links',
            'meta-tableList': 'Tb,List,Form+',
                'meta-table': 'Table+',
                'meta-list': 'List+',
                'tableHtml':'Table Start', 
                'tableCol': 'Table Column', 
                'tableRow': 'Table Row',
                'listItem': 'List Item',
                'listUO': 'Unordered List', 
                'listOU': 'Ordered List', 

            'meta-formating': 'Txt Format+',
                'supersubHtml': 'Supersubscpt',
                'boldHtml': 'Bold',
                'italicHtml': 'Italic',
                'subscriptHtml': 'Subscript',
                'quoteHtml': 'Quote',
                'insertedHtml': 'Inserted Txt',
                'delHtml': 'Deleted Txt',
                'smallHtml': 'Small Txt',
            'meta-action': 'Actions+', 
            'meta-elements': 'Elements+',
            'meta-media': 'Img, Media+',
                'html5Supp': 'Html5 Support',
                'imageHtml': 'Image',
                'videoHtml': 'Video',
                'svgHtml': 'SVG',
                'audioHtml': 'Audio',
                'canvasHtml': 'Canvas',
                'iframeHtml': 'iframe',
                'objectHtml': 'Object html',
            'meta-block': 'Blocks+',
                'divHtml': 'div tag',
                'spanHtml': 'span tag',
                'textareaHtml': 'Text area',
                'lineBreak': 'Line break',
                'highlightHtml': 'Highlight txt',
                'commentHtml': 'Comment',
            'meta-input':'Inputs+',
            'meta-button': 'Buttons+',
                'buttonHtml': 'Buttons',
            'meta-inputOthers': 'Other Inputs+',
                'inputText': 'Input Text',
                'inputNum': 'Input #',
                'inputSubmit': 'Submit Input',
                'inputEmail': 'Input Email',
                'inputCheckbox': 'Input Chckbx',
                'inputRadio': 'Input Radio',
            'codeHtml': 'Code tag',           
       
            //css keyboard
            'meta-css': 'CSS kb+',
            'meta-moreCSS': 'Start CSS+',
            'meta-position': 'Positioning+', 
                'meta-positionType':'Pos. Type+', 
                'positionCSS':'Position:', 
                'leftCSS':'left:',
                'rightCSS':'right:',
                'widthCSS':'width:',  
                'heightCSS':'height:', 
                'bottomCSS':'bottom:', 
                'topCSS':'top:',
                'zindexCSS':'zindex', 
                'floatCSS':'float:', 
            'meta-border': 'Border+',
            'meta-background' : 'Background+', 
            'meta-margin': 'Margin+',
                'marginCSS':'margin:', 
                'paddingCSS':'padding:',
                'borderCSS':'border:',   
                'meta-outline':'outline:',   
                    'meta-moreOutline':'More Outlines+', 
                    'outlineStyleCSS':'Outline style', 
                    'outlineColorCSS':'Outline Color', 
            'meta-color': 'Colors+',
                'meta-moreColor':'More Colors+', 
                'colorCSS':'Font Color',
            'meta-transfTransit': 'Movements+',
            'meta-fontText': 'Font, Text+',         
                'meta-fontType':'Font Type+',
                'fontCSS':'Font:', 
                'fontFamCSS':'Font Family',
                'fontSizeCSS':'Font Size',
                'fontVarCSS':'Font Variant', 
                'fontWeightCSS':'Font Weight', 
                'fontStyleCSS': 'Font Style',
            
                'meta-textType':'Text Type+',
                'textCSS':'Text',
                'meta-textAlign':'Text Align+',
                    'textAlign':'Text Align:', 
                    'direcCSS':'Direction',
                    'verticalAlign':'Vertical Align',
                'unicodeBidi':'UnicodeBidi',
                'textIndent':'Indent Text', 
                'textDecor':'Text Decor',
                'textShadow':'Text Shadow',
                'meta-textSpace':'Text Space+', 
                    'letterSpace':'Letter Space', 
                    'wordSpace':'Word Space',
                    'whiteSpace':'White Space',
                'textOver':'Text Overflow', 
                'textTransform':'Text Transform',
            'meta-Boxes': 'Boxes+',
            'meta-hover': 'Hover+',
                'hoverCSS': 'hover:',
            'meta-animate':'Animations+',
                'animationCSS':'Animate:',
                'animationDelay':'Animat delay',
                'animationName':'Animat name', 
                'animationDuration':'Animat Dura', 
                'animationDirec':'Animat Direct',        
            'meta-transition':'transitions+',
            'transTCSS':'Transition:', 
            'transFCSS':'Transform:',

            'meta-transform3D': '3D Transform+',
                'transFDelay': 'Trans Delay',
                'transFDura':'Trans Duration',
                'transFProp':'Trans Property',
                'transTTimin':'Trans Timing',
            'meta-transform': 'Transform+',
                'meta-skew': 'Skew',
                'meta-scale':'Scaling',
                'meta-rotate':'Rotations',
                'meta-translate': 'Translations',
                    'transFOrigin':'Trans Origin', 
                    'transFStyle':'Trans Style',
                    'perspectCSS':'Perspective',
                    'backfaceVisible':'Backface Visi', 
                    'perspectOrigin':'Perspec Orign',
            'meta-transition': 'Transition+',
            'meta-translate': 'Translation+',  
            'styleTag': 'style tag',
            'meta-visiblity': 'Visibility+',
                'opacityCSS': 'opacity:',
                'filterCSS':'filter',
            'meta-webkit': 'webkit...',

            //javascript keyboard
            'scriptHtml': 'Script tag',
            'meta-javas': 'Javascrpt kb+',
            'meta-moreJS': 'JS Start+',
            'meta-varString': 'Var, objects+', 
                'varJS': 'variable',
                'stringJS':'String JS',
                'arrayJS':'array',
            'meta-math': 'Math+',
                'meta-mathOperators':'Math Ops+', 
                'meta-mathComparisons':'Comparison+',
                'meta-mathFunc':'Math Functions+',
            'meta-events': 'Events+', 
                'meta-domEvents':'Dom Events', 
                'meta-onmouse':'Onmouse', 
                'meta-onkey':'Onkey',                    
            'meta-comparisons': 'Comparison+', 
            'meta-methods': 'Methods+',
                'meta-numMethods': '# Methods+',
                    'meta-numProp':'Num Properties',  
                'meta-stringMethods': 'Strng Mthds+',   
            'meta-loops': 'Loops+',
                'forLoop': 'For loop',
                'whileLoop':'While loop',
                'meta-returnType':'Return Type', 
                'switchJS': 'Switch',
                'tryCatchLoop':'Try Catch', 
                'dowhileLoop': 'Do While',
            'meta-elements': 'Elements+',
                'meta-getElements':'getElem+',
                'meta-changeElements': 'Change Elem+',
                'meta-addDeleteElements': '+/- Elem+',
            'meta-advJS':'Advanced JS+', 
            'meta-nav2': 'English Nav+',
            'meta-nav3': 'HTML Nav+',
            'meta-nav4': 'CSS Nav+',
            'meta-nav5': 'Javascrpt Nav+',

            //custom codemirror action function buttons
            'cmBksp': 'Backspace', //Codemirror backspace
            'cmLineUp': 'Line down', //Codemirror Lineup
            'cmLineDown': 'Line up', //Codemirror Linedown
            'cmAutocomplete': 'Autocomplete', //Codemirror Autocomplete
            'cmJump': 'Jump to Line', //Codemirror Jump to line
            'cmDeleteL': 'Delete Line',//CM delete line
            'cmDeleteRight': 'Delete Right',//CM delete char to right
            'cmUndo': 'Undo',//CM undo
            'cmRedo': 'Redo',//CM redo
            'cmSelectAll': 'SelectAll',//CM select all
            'cmSingleSelection': 'Single Select',//CM redo
            'cmReset': 'Reset',//CM reset
            //other custom action function names

            //end of custom names


            // check mark (accept)
            'a': '\u2714:Accept (Shift-Enter)',
            'accept': 'Accept:Accept (Shift-Enter)',
            'alt': 'AltGr:Alternate Graphemes',
            // Left arrow (same as &larr;)
            'b': '\u2190:Backspace',
            'bksp': 'Bksp:Backspace',
            // big X, close/cancel
            'c': '\u2716:Cancel (Esc)',
            'cancel': 'Cancel:Cancel (Esc)',
            // clear num pad
            'clear': 'C:Clear',
            'combo': '\u00f6:Toggle Combo Keys',
            // num pad decimal '.' (US) & ',' (EU)
            'dec': '.:Decimal',
            // down, then left arrow - enter symbol
            'e': '\u21b5:Enter',
            'empty': '\u00a0', // &nbsp;
            'enter': 'Enter:Enter',
            // left arrow (move caret)
            'left': '\u2190',
            // caps lock
            'lock': '\u21ea Lock:Caps Lock',
            'next': 'Next \u21e8',
            'prev': '\u21e6 Prev',
            // right arrow (move caret)
            'right': '\u2192',
            // thick hollow up arrow
            's': '\u21e7:Shift',
            'shift': 'Shift:Shift',
            // +/- sign for num pad
            'sign': '\u00b1:Change Sign',
            'space': 'space:Space',
            // right arrow to bar
            // \u21b9 is the true tab symbol
            't': '\u21e5:Tab',
            'tab': '\u21e5 Tab:Tab',
            // replaced by an image
            'toggle': ' ',

            // added to titles of keys
            // accept key status when acceptValid:true
            'valid': 'valid',
            'invalid': 'invalid',
            // combo key states
            'active': 'active',
            'disabled': 'disabled'
        },

        // Message added to the key title while hovering,
        // if the mousewheel plugin exists
        wheelMessage: 'Use mousewheel to see other keys',

        css: {
            // input & preview
            input: 'ui-widget-content ui-corner-all',
            // keyboard container
            container: 'ui-widget-content ui-widget ui-corner-all ui-helper-clearfix',
            // keyboard container extra class (same as container, but separate)
            popup: '',
            // default state
            buttonDefault: 'ui-state-default ui-corner-all',
            // hovered button
            buttonHover: 'ui-state-hover',
            // Action keys (e.g. Accept, Cancel, Tab, etc);
            // this replaces "actionClass" option
            buttonAction: 'ui-state-active',
            // Active keys
            // (e.g. shift down, meta keyset active, combo keys active)
            buttonActive: 'ui-state-active',
            // used when disabling the decimal button {dec}
            // when a decimal exists in the input area
            buttonDisabled: 'ui-state-disabled',
            // {empty} button class name
            buttonEmpty: 'ui-keyboard-empty'
        },

        // *** Useability ***
        // Auto-accept content when clicking outside the
        // keyboard (popup will close)
        autoAccept: false,
        // Auto-accept content even if the user presses escape
        // (only works if `autoAccept` is `true`)
        autoAcceptOnEsc: false,

        // Prevents direct input in the preview window when true
        lockInput: false,

        // Prevent keys not in the displayed keyboard from being
        // typed in
        restrictInput: false,
        // Additional allowed characters while restrictInput is true
        restrictInclude: '', // e.g. 'a b foo \ud83d\ude38'

        // Check input against validate function, if valid the
        // accept button is clickable; if invalid, the accept
        // button is disabled.
        acceptValid: true,
        // Auto-accept when input is valid; requires `acceptValid`
        // set `true` & validate callback
        autoAcceptOnValid: false,

        // if acceptValid is true & the validate function returns
        // a false, this option will cancel a keyboard close only
        // after the accept button is pressed
        cancelClose: true,

        // tab to go to next, shift-tab for previous
        // (default behavior)
        tabNavigation: false,

        // enter for next input; shift-enter accepts content &
        // goes to next shift + "enterMod" + enter ("enterMod"
        // is the alt as set below) will accept content and go
        // to previous in a textarea
        enterNavigation: false,
        // mod key options: 'ctrlKey', 'shiftKey', 'altKey',
        // 'metaKey' (MAC only)
        // alt-enter to go to previous;
        // shift-alt-enter to accept & go to previous
        enterMod: 'altKey',

        // if true, the next button will stop on the last
        // keyboard input/textarea; prev button stops at first
        // if false, the next button will wrap to target the
        // first input/textarea; prev will go to the last
        stopAtEnd: true,

        // Set this to append the keyboard immediately after the
        // input/textarea it is attached to. This option works
        // best when the input container doesn't have a set width
        // and when the "tabNavigation" option is true
        appendLocally: false,
        // When appendLocally is false, the keyboard will be appended
        // to this object
        appendTo: 'body',

        // If false, the shift key will remain active until the
        // next key is (mouse) clicked on; if true it will stay
        // active until pressed again
        stickyShift: true,

        // Prevent pasting content into the area
        preventPaste: false,

        // caret places at the end of any text
        caretToEnd: false,

        // caret stays this many pixels from the edge of the input
        // while scrolling left/right; use "c" or "center" to center
        // the caret while scrolling
        scrollAdjustment: 10,

        // Set the max number of characters allowed in the input,
        // setting it to false disables this option
        maxLength: false,
        // allow inserting characters @ caret when maxLength is set
        maxInsert: true,

        // Mouse repeat delay - when clicking/touching a virtual
        // keyboard key, after this delay the key will start
        // repeating
        repeatDelay: 500,

        // Mouse repeat rate - after the repeatDelay, this is the
        // rate (characters per second) at which the key is
        // repeated. Added to simulate holding down a real keyboard
        // key and having it repeat. I haven't calculated the upper
        // limit of this rate, but it is limited to how fast the
        // javascript can process the keys. And for me, in Firefox,
        // it's around 20.
        repeatRate: 20,

        // resets the keyboard to the default keyset when visible
        resetDefault: false,

        // Event (namespaced) on the input to reveal the keyboard.
        // To disable it, just set it to ''.
        openOn: 'focus',

        // Event (namepaced) for when the character is added to the
        // input (clicking on the keyboard)
        keyBinding: 'mousedown touchstart',

        // enable/disable mousewheel functionality
        // enabling still depends on the mousewheel plugin
        useWheel: true,

        // combos (emulate dead keys)
        // if user inputs `a the script converts it to à,
        // ^o becomes ô, etc.
        useCombos: true,
        // if you add a new combo, you will need to update the
        // regex below
        combos: {
            // uncomment out the next line, then read the Combos
            //Regex section below
            '<': { 3: '\u2665' }, // turn <3 into ♥ - change regex below
            'a': { e: "\u00e6" }, // ae ligature
            'A': { E: "\u00c6" },
            'o': { e: "\u0153" }, // oe ligature
            'O': { E: "\u0152" }
        },

        // *** Methods ***
        // Callbacks - attach a function to any of these
        // callbacks as desired
        initialized: function (e, keyboard, el) { },
        beforeVisible: function (e, keyboard, el) { },
        visible: function (e, keyboard, el) { },
        beforeInsert: function (e, keyboard, el, textToAdd) { return textToAdd; },
        change: function (e, keyboard, el) { },
        beforeClose: function (e, keyboard, el, accepted) { },
        accepted: function (e, keyboard, el) { },
        canceled: function (e, keyboard, el) { },
        restricted: function (e, keyboard, el) { },
        hidden: function (e, keyboard, el) { },

        // called instead of base.switchInput
        // Go to next or prev inputs
        // goToNext = true, then go to next input;
        //   if false go to prev
        // isAccepted is from autoAccept option or
        //   true if user presses shift-enter
        switchInput: function (keyboard, goToNext, isAccepted) { },

        /*
                // build key callback
            buildKey : function( keyboard, data ) {
                  /* data = {
                    // READ ONLY
                    isAction: [boolean] true if key is an action key
                  // name... may include decimal ascii value of character
                    // prefix = 'ui-keyboard-'
                    name    : [string]  key class name suffix
                    value   : [string]  text inserted (non-action keys)
                    title   : [string]  title attribute of key
                    action  : [string]  keyaction name
                    // html includes a <span> wrapping the text
                    html    : [string]  HTML of the key;
                    // DO NOT MODIFY THE ABOVE SETTINGS
  
                  // use to modify key HTML
                    $key    : [object]  jQuery selector of key already added to keyboard
                  }
                  * /
                  data.$key.html('<span class="ui-keyboard-text">Foo</span>');
                  return data;
                  },
                */

        // this callback is called just before the "beforeClose"
        // to check the value if the value is valid, return true
        // and the keyboard will continue as it should (close if
        // not always open, etc)
        // if the value is not value, return false and the clear
        // the keyboard value ( like this
        // "keyboard.$preview.val('');" ), if desired
        validate: function (keyboard, value, isClosing) {
            return true;
        }

    })
      // activate the typing extension
      .addTyping({
          showTyping: true,
          delay: 250
      });

    /* Combos Regex -
      You could change $.keyboard.comboRegex so that it applies to all
      keyboards, but if a specific layout language has a comboRegex defined,
      it has precidence over this setting. This regex is used to match combos
      to combine, the first part looks for the accent/letter and the second
      part matches the following letter so lets say you want to do something
      crazy like turn "<3" into a heart. Add this to the
      combos '<' : { 3: "\u2665" } and add a comma to the end if needed.
      Then change the regex to this: /([<`\'~\^\"ao])([a-z3])/mig;
                                             ( first part  )( 2nd  )  */
    $.keyboard.language.love.comboRegex = /([<`\'~\^\"ao])([a-z3])/mig;

    /* if you look close at the regex, all that was added was "<" to the
      beginning of the first part; some characters need to be escaped with a
      backslash in front because they mean something else in regex. The
      second part has a 3 added after the 'a-z', so that should cover both
      parts :P */

    // Typing Extension
    $('#icon').click(function () {
        var kb = $('#keyboard').getkeyboard();
        // typeIn( text, delay, callback );
        kb.reveal().typeIn(simulateTyping, 500, function () {
            // do something after text is added
            // kb.accept();
        });
    });

});
