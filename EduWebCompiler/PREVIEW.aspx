﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Preview.aspx.cs" Inherits="EduWebCompiler.Preview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 279px;
        }
        .auto-style3 {
            margin-top: 0px;
        }
        .auto-style4 {
            width: 508px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        Tutorial Files<br />
        Select files (click on the file in the select file column, then click accept):<br />
        <br />
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <asp:GridView ID="GridView2" runat="server" DataSourceID="SqlDataSource2" Height="216px" Width="533px" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="html_id">
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
                            <asp:BoundField DataField="html_id" HeaderText="html_id" InsertVisible="False" ReadOnly="True" SortExpression="html_id" />
                            <asp:BoundField DataField="category" HeaderText="category" SortExpression="category" />
                            <asp:BoundField DataField="filename" HeaderText="filename" SortExpression="filename" />
                            <asp:BoundField DataField="filelocation" HeaderText="filelocation" SortExpression="filelocation" />
                            <asp:HyperLinkField DataNavigateUrlFields="filename" DataNavigateUrlFormatString="Preview.aspx?filename={0}" HeaderText="Select File" Text="File" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:htmlConnectionString3 %>" DeleteCommand="DELETE FROM [htmlTable] WHERE [html_id] = @original_html_id AND (([category] = @original_category) OR ([category] IS NULL AND @original_category IS NULL)) AND (([filename] = @original_filename) OR ([filename] IS NULL AND @original_filename IS NULL)) AND (([filelocation] = @original_filelocation) OR ([filelocation] IS NULL AND @original_filelocation IS NULL))" InsertCommand="INSERT INTO [htmlTable] ([category], [filename], [filelocation]) VALUES (@category, @filename, @filelocation)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [htmlTable]" UpdateCommand="UPDATE [htmlTable] SET [category] = @category, [filename] = @filename, [filelocation] = @filelocation WHERE [html_id] = @original_html_id AND (([category] = @original_category) OR ([category] IS NULL AND @original_category IS NULL)) AND (([filename] = @original_filename) OR ([filename] IS NULL AND @original_filename IS NULL)) AND (([filelocation] = @original_filelocation) OR ([filelocation] IS NULL AND @original_filelocation IS NULL))">
                        <DeleteParameters>
                            <asp:Parameter Name="original_html_id" Type="Decimal" />
                            <asp:Parameter Name="original_category" Type="String" />
                            <asp:Parameter Name="original_filename" Type="String" />
                            <asp:Parameter Name="original_filelocation" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="category" Type="String" />
                            <asp:Parameter Name="filename" Type="String" />
                            <asp:Parameter Name="filelocation" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="category" Type="String" />
                            <asp:Parameter Name="filename" Type="String" />
                            <asp:Parameter Name="filelocation" Type="String" />
                            <asp:Parameter Name="original_html_id" Type="Decimal" />
                            <asp:Parameter Name="original_category" Type="String" />
                            <asp:Parameter Name="original_filename" Type="String" />
                            <asp:Parameter Name="original_filelocation" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <br />
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" CssClass="auto-style3" Height="267px" Width="760px"></asp:TextBox>
                    <br />
                </td>
                <td>
                    <br />
                </td>
            </tr>
        </table>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="clickTutorials" Text="Accept" />
        <br />
        <br />
        <br />
        Add files (include extension like .txt or .html):<br />
        <br />
        Create new row for tutorials: category (name displayed on webpage): <asp:TextBox ID="categoryText" runat="server" Width="82px"></asp:TextBox>
        &nbsp;<asp:Button ID="Insert" runat="server" Text="New row" Width="92px" OnClick="insertHtmlTable" />
        <br />
        <br />
        <br />
        Saved files from website:<br />
        <br />
        <table class="auto-style1">
            <tr>
                <td class="auto-style4">
                    <asp:GridView ID="GridView3" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="html_id" DataSourceID="SqlDataSource1" Width="544px">
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
                            <asp:BoundField DataField="html_id" HeaderText="html_id" InsertVisible="False" ReadOnly="True" SortExpression="html_id" />
                            <asp:BoundField DataField="category_Saved" HeaderText="category_Saved" SortExpression="category_Saved" />
                            <asp:BoundField DataField="filename" HeaderText="filename" SortExpression="filename" />
                            <asp:BoundField DataField="filelocation" HeaderText="filelocation" SortExpression="filelocation" />
                            <asp:HyperLinkField DataNavigateUrlFields="filename" DataNavigateUrlFormatString="Preview.aspx?filename={0}" HeaderText="Select File" Text="File" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:htmlConnectionString4 %>" DeleteCommand="DELETE FROM [savedHtml] WHERE [html_id] = @original_html_id AND (([category_Saved] = @original_category_Saved) OR ([category_Saved] IS NULL AND @original_category_Saved IS NULL)) AND (([filename] = @original_filename) OR ([filename] IS NULL AND @original_filename IS NULL)) AND (([filelocation] = @original_filelocation) OR ([filelocation] IS NULL AND @original_filelocation IS NULL))" InsertCommand="INSERT INTO [savedHtml] ([category_Saved], [filename], [filelocation]) VALUES (@category_Saved, @filename, @filelocation)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [savedHtml]" UpdateCommand="UPDATE [savedHtml] SET [category_Saved] = @category_Saved, [filename] = @filename, [filelocation] = @filelocation WHERE [html_id] = @original_html_id AND (([category_Saved] = @original_category_Saved) OR ([category_Saved] IS NULL AND @original_category_Saved IS NULL)) AND (([filename] = @original_filename) OR ([filename] IS NULL AND @original_filename IS NULL)) AND (([filelocation] = @original_filelocation) OR ([filelocation] IS NULL AND @original_filelocation IS NULL))">
                        <DeleteParameters>
                            <asp:Parameter Name="original_html_id" Type="Decimal" />
                            <asp:Parameter Name="original_category_Saved" Type="String" />
                            <asp:Parameter Name="original_filename" Type="String" />
                            <asp:Parameter Name="original_filelocation" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="category_Saved" Type="String" />
                            <asp:Parameter Name="filename" Type="String" />
                            <asp:Parameter Name="filelocation" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="category_Saved" Type="String" />
                            <asp:Parameter Name="filename" Type="String" />
                            <asp:Parameter Name="filelocation" Type="String" />
                            <asp:Parameter Name="original_html_id" Type="Decimal" />
                            <asp:Parameter Name="original_category_Saved" Type="String" />
                            <asp:Parameter Name="original_filename" Type="String" />
                            <asp:Parameter Name="original_filelocation" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <br />
                    <br />
                </td>
                <td>
                    <br />
                    <br />
                    <asp:TextBox ID="TextBox2" runat="server" Height="282px" TextMode="MultiLine" Width="765px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" OnClick="clickSavedFiles" Text="Accept" />
        <br />
        <br />
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
